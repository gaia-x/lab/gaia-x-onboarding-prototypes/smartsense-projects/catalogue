package eu.gaiax.federatedcatalogue.rest;

import com.smartsensesolutions.java.commons.FilterRequest;
import eu.gaiax.federatedcatalogue.model.request.RecordFilter;
import eu.gaiax.federatedcatalogue.model.response.CataloguePage;
import eu.gaiax.federatedcatalogue.model.response.CommonResponse;
import eu.gaiax.federatedcatalogue.model.response.ServiceDetailResponse;
import eu.gaiax.federatedcatalogue.model.response.ServiceListResponse;
import eu.gaiax.federatedcatalogue.service.neo4j.search.CatalogueSearchService;
import eu.gaiax.federatedcatalogue.service.neo4j.serviceoffering.ServiceOfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static eu.gaiax.federatedcatalogue.utils.constant.ApplicationRestConstant.GAIA_X_BASE_PATH;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = GAIA_X_BASE_PATH)
@RequiredArgsConstructor
public class CatalogueSearchResource {

    private final CatalogueSearchService catalogueSearchService;

    private final ServiceOfferService serviceOfferService;

    @GetMapping(value = "/catalogue/selector", produces = APPLICATION_JSON_VALUE)
    public List<Map<String, Object>> getAllNodes() {
        return catalogueSearchService.getNodeLabelsAndRelationships();
    }

    @GetMapping(value = "/catalogue/grammar", produces = "application/javascript")
    public String getGrammar() {
        return catalogueSearchService.getGrammarContent();
    }

    @PostMapping(value = "/catalogue/selector/option/{option}/property/{property}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public CataloguePage<String> getValues(@PathVariable("option") String option, @PathVariable("property") String property, @RequestBody FilterRequest filterRequest) {
        return catalogueSearchService.getOptions(option, property, filterRequest);
    }

    @PostMapping(value = "/catalogue/search", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public CataloguePage<ServiceListResponse> executeQuery(@RequestBody RecordFilter filter) {
        return serviceOfferService.getSearchData(filter);
    }

    @GetMapping(value = "/catalogue/service-details", produces = APPLICATION_JSON_VALUE)
    public CommonResponse<ServiceDetailResponse> getServiceDetailsById(@RequestParam(value = "id") UUID id) {
        return CommonResponse.of(serviceOfferService.getServiceDetailsById(id));
    }
}
